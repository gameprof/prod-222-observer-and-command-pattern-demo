using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractionSubject {
    void RegisterInteraction( System.Action observerInteraction );
    void UnRegisterInteraction( System.Action observerInteraction );
}
