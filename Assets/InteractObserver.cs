using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class InteractObserver : MonoBehaviour {
    public GameObject interactMarker;

    void Start() {
        interactMarker.SetActive( false );
    }
    
    private void OnTriggerEnter( Collider other ) {
        IInteractionSubject subject = other.GetComponentInParent<IInteractionSubject>();
        if ( subject != null ) {
            subject.RegisterInteraction(Interact);
            interactMarker.SetActive( true );
        }
    }
    
    private void OnTriggerExit( Collider other ) {
        IInteractionSubject subject = other.GetComponentInParent<IInteractionSubject>();
        if ( subject != null ) {
            subject.UnRegisterInteraction(Interact);
            interactMarker.SetActive( false );
        }
    }

    void Interact() {
        Debug.Log( $"You clicked {gameObject.name}" );
    }
}
