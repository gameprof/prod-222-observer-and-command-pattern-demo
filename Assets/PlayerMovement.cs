using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour, IInteractionSubject {
    static public event System.Action InteractActionEvent;
    // This is an example of how to add a method to the event above manually:
    // PlayerMovement.InteractActionEvent += Interact;
    
    
    public float speed        = 10;
    public float jumpVel      = 10;
    public float rotMultYaw   = 90;
    public float rotMultPitch = 45;
    public float pitchLimit   = 60;
    [FormerlySerializedAs( "framesBeforeRotation" )]
    public int   framesBeforeInput = 5;

    public List<ButtonAction> buttonActions;
    
    private Rigidbody rigid;
    private Transform camTrans;

    // Start is called before the first frame update
    void Start() {
        rigid = GetComponent<Rigidbody>();
        camTrans = GetComponentInChildren<Camera>().transform;
        if ( camTrans == null ) {
            Debug.LogError( $"Couldn't find Camera on {gameObject.name}" );
        }
        
        Cursor.lockState = CursorLockMode.Locked; // Lock the cursor to the center
        Cursor.visible = false; // Hide the cursor
    }

    // Update is called once per frame
    void Update() {
        if ( framesBeforeInput > 0 ) {
            framesBeforeInput--;
            return;
        }
        
        // Look around
        float mouseYaw = Input.GetAxis( "Mouse X" ) * rotMultYaw;
        float mousePitch = Input.GetAxis( "Mouse Y" ) * rotMultPitch;
        Vector3 rot = transform.eulerAngles;
        rot.y += mouseYaw * Time.deltaTime;
        transform.eulerAngles = rot;
        Vector3 camRot = camTrans.eulerAngles;
        camRot.x += mousePitch * Time.deltaTime;
        if ( camRot.x > 180 ) {
            camRot.x -= 360;
        }
        camRot.x = Mathf.Clamp( camRot.x, -pitchLimit, pitchLimit );
        camTrans.eulerAngles = camRot;
        
        
        // Move relative to our look direction
        float yVel = rigid.velocity.y;

        
        Vector3 vel = Vector3.zero;
        vel += transform.right * Input.GetAxis( "Horizontal" );
        vel += transform.forward * Input.GetAxis( "Vertical" );
        if (vel.magnitude > 1) vel.Normalize();
        vel *= speed;
        vel.y = yVel;
        rigid.velocity = vel;
        
        // if ( Input.GetKeyDown( KeyCode.LeftShift ) || Input.GetMouseButtonDown( 1 ) ) {
        //     Interact();
        // }
        // if ( Input.GetKeyDown( KeyCode.Space ) || Input.GetMouseButtonDown(0) ) {
        //     Jump();
        // }

        foreach ( ButtonAction buttonAction in buttonActions ) {
            if ( Input.GetKeyDown( buttonAction.keyToWatch )
                 || Input.GetMouseButtonDown( buttonAction.mouseButtonNum ) ) {
                buttonAction.methodToCall.Invoke();
            }
        }
    }

    public void RegisterInteraction( Action observerInteraction ) {
        InteractActionEvent -= observerInteraction;
        InteractActionEvent += observerInteraction;
    }

    public void UnRegisterInteraction( Action observerInteraction ) {
        InteractActionEvent -= observerInteraction;
    }

    public void Jump() {
        Vector3 vel = rigid.velocity;
        vel.y = jumpVel;
        rigid.velocity = vel;
    }

    public void Interact() {
        InteractActionEvent?.Invoke();
    }
}


[System.Serializable]
public class ButtonAction {
    public int                           mouseButtonNum = 0;
    public KeyCode                       keyToWatch;
    public UnityEngine.Events.UnityEvent methodToCall;
}
